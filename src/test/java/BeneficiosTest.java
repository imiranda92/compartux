import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import static io.appium.java_client.touch.offset.PointOption.point;
import static org.testng.Assert.*;

public class BeneficiosTest extends TestBase{

    public String path= "/Users/gesfor-g0088/Desktop/EvidenciasComparTfon/Beneficios/";

    @BeforeClass
    public void setUP() throws IOException {
        File file = new File(path);
        FileUtils.deleteDirectory(file);
        java.io.File folder = new File(path);
        folder.mkdir();

    }

    @Test
    public void consultaDetalleDescuento() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(new MobileBy.ByAccessibilityId("Open navigation drawer")));
        MobileElement el1 = (MobileElement) ad.findElementByAccessibilityId("Open navigation drawer");
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/benefits_menu_btn")));
        MobileElement el01 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/benefits_menu_btn"));
        Thread.sleep(200);
        el01.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget." +
                "LinearLayout/android.widget.TextView[1]")));

        //Por ser cliente compartamos
        MobileElement el3 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/andro" +
                        "id.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]"));
        Assert.assertTrue(el3.getText().equals("por ser Cliente Compartamos."));

        //Por incrementar el monto
        MobileElement el4 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.FrameLayout" +
                "/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]"));
        Assert.assertTrue(el4.getText().equals("por incrementar en $1,000 el monto de tu crédito Compartamos."));

        //Por ser miembro comite
        MobileElement el5 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.FrameLayout/android.widget." +
                "LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]"));
        Assert.assertTrue(el5.getText().equals("por ser miembro del comité."));

        //Por antiguedad
        MobileElement el6 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget." +
                "LinearLayout/android.widget.TextView[1]"));
        Assert.assertTrue(el6.isDisplayed());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void consultaDescuento() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(new MobileBy.ByAccessibilityId("Open navigation drawer")));
        MobileElement el1 = (MobileElement) ad.findElementByAccessibilityId("Open navigation drawer");
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/benefits_menu_btn")));
        MobileElement el01 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/benefits_menu_btn"));
        Thread.sleep(200);
        el01.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/benefits_discount_txt")));
        //Descuento
        MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/benefits_discount_txt");
        Assert.assertTrue((Integer.parseInt(el2.getText().substring(0,2))>30));
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }


}