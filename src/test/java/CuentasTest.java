import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import static io.appium.java_client.touch.offset.PointOption.point;

public class CuentasTest extends TestBase{

    public String path= "/Users/gesfor-g0088/Desktop/EvidenciasComparTfon/Cuentas/";

    @BeforeClass
    public void setUP() throws IOException {
        File file = new File(path);
        FileUtils.deleteDirectory(file);
        java.io.File folder = new File(path);
        folder.mkdir();

    }
    @Test
    public void validaSeleccionCuenta() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Assert.assertTrue(el1.getText().equals("Cuenta a mi Favor")|| el1.getText().equals("Mis Ahorros Compartamos"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_balance")));
        MobileElement el2 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_balance"));
        Assert.assertTrue(el2.isDisplayed());
        MobileElement el3 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/img_account_type"));
        Assert.assertTrue(el3.isDisplayed());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void validaConsultaSaldo() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/accounts_account_money")));
        MobileElement el2 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/accounts_account_money"));
        MobileElement el3 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/accounts_account_txt_sign"));
        MobileElement el4 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/accounts_account_txt_mn"));
        Assert.assertTrue(el2.isDisplayed());
        Assert.assertTrue(el3.isDisplayed());
        Assert.assertTrue(el4.isDisplayed());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void consultaMovimientoCuenta() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));
        MobileElement el2 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]"));
        MobileElement el3 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
        MobileElement el4 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView\n"));
        Assert.assertTrue(el2.isDisplayed());
        Assert.assertTrue(el3.isDisplayed());
        Assert.assertTrue(el4.isDisplayed());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }


    @Test
    public void validaFiltroMovimientos() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();



        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));
        MobileElement el2 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]"));
        MobileElement el3 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
        MobileElement el4 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView\n"));
        Assert.assertTrue(el2.isDisplayed());
        Assert.assertTrue(el3.isDisplayed());
        Assert.assertTrue(el4.isDisplayed());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }


    @Test
    public void validaAhorrosCuenta() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(new MobileBy.ByAccessibilityId("Ahorros")));
        MobileElement el2 = (MobileElement) ad.findElementByAccessibilityId("Ahorros");
        el2.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/linearLayout2")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.widget.LinearLayout/" +
                        "android.widget.TextView[2]")));
        MobileElement el3 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.widget.LinearLayout/android.widget.TextView[2]"));

        MobileElement el4= (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.widget.LinearLayout/android.widget.TextView[1]"));
        Assert.assertTrue(el3.getText().equals("Ahorros"));
        Assert.assertTrue(el4.getText().equals("Mes"));
        MobileElement el5 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout/android.widg" +
                "et.TextView[3]"));
        MobileElement el6 = (MobileElement) ad.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]"));

        Assert.assertTrue(el5.isDisplayed());
        Assert.assertTrue(el6.isDisplayed());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void validaDetelleCuenta() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));


        wait.until(ExpectedConditions.visibilityOfElementLocated(new MobileBy.ByAccessibilityId("Información")));
        MobileElement el2 = (MobileElement) ad.findElementByAccessibilityId("Información");
        el2.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/accounts_info_txt_number")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/accounts_info_clabe")));

        MobileElement el3 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/accounts_info_txt_number"));
        MobileElement el4 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/accounts_info_number"));
        MobileElement el5 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/accounts_info_txt_clabe"));
        MobileElement el6 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/accounts_info_clabe"));


        Assert.assertTrue(el3.getText().equals("Cuenta"));
        Assert.assertTrue(el5.getText().equals("CLABE"));
        Assert.assertTrue(el3.getText().equals("Cuenta"));
        Assert.assertTrue(el4.getText().contains("001"));
        Assert.assertTrue(el6.isDisplayed());

        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }


}