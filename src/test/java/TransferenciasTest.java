import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

public class TransferenciasTest extends TestBase{

    public String path= "/Users/gesfor-g0088/Desktop/EvidenciasComparTfon/Transferencias/";

    @BeforeClass
    public void setUP() throws IOException {
        File file = new File(path);
        FileUtils.deleteDirectory(file);
        java.io.File folder = new File(path);
        folder.mkdir();

    }
    @Test
    public void validaMonto() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/account_btn_transfer")));
        MobileElement el2 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_btn_transfer"));
        el2.click();


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit")));
        MobileElement el3 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit"));
        el3.sendKeys("0.00");

        MobileElement el7 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_number_edit"));
        el7.sendKeys(ctaDestino);
        Thread.sleep(400);

        MobileElement el4 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfer_confirm_button"));
        Assert.assertFalse(el4.isEnabled());

        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void validaCampoCuenta() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/account_btn_transfer")));
        MobileElement el2 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_btn_transfer"));
        el2.click();


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit")));
        MobileElement el3 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit"));
        el3.sendKeys("0.01");

        MobileElement el7 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_number_edit"));
        el7.sendKeys("09876543210987665432143");
        Thread.sleep(1000);

        System.out.println(el7.getText().length());
        //MobileElement el4 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfer_confirm_button"));
        Assert.assertTrue(el7.getText().length()==11);


        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void transferenciaEnCuentas() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/account_btn_transfer")));
        MobileElement el2 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_btn_transfer"));
        el2.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit")));
        MobileElement el3 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit"));
        el3.sendKeys("0.01");
        MobileElement el7 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_number_edit"));
        el7.sendKeys(ctaDestino);
        Thread.sleep(400);

        MobileElement el4 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfer_confirm_button"));
        el4.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/transfer_btn_confirm")));
        MobileElement el5 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfer_btn_confirm"));
        el5.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/ticket_title")));
        MobileElement el6 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/ticket_title"));
        Assert.assertTrue(el6.getText().equals("¡Transferencia exitosa!"));

        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void transferenciaCuentaErronea() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/account_btn_transfer")));
        MobileElement el2 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_btn_transfer"));
        el2.click();


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit")));
        MobileElement el3 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit"));
        el3.sendKeys("0.01");

        MobileElement el7 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_number_edit"));
        el7.sendKeys("098765432109876654");
        Thread.sleep(300);
        MobileElement el4 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfer_confirm_button"));
        el4.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/transfer_btn_confirm")));
        MobileElement el5 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfer_btn_confirm"));
        el5.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/loader_text")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/transfer_btn_confirm")));
        MobileElement el6 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfer_btn_confirm"));
        Assert.assertTrue(el6.isEnabled());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void transferenciaEnMenu() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(new MobileBy.ByAccessibilityId("Open navigation drawer")));
        MobileElement el0 = (MobileElement) ad.findElementByAccessibilityId("Open navigation drawer");
        el0.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/transfers_menu_btn")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfers_menu_btn"));
        Thread.sleep(200);
        el1.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit")));
        MobileElement el3 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit"));
        el3.sendKeys("0.01");
        MobileElement el7 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_number_edit"));
        el7.sendKeys(ctaDestino);

        MobileElement el8 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/root_account_edit");
        el8.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/bundles_accounts_amount")));
        MobileElement el9 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/bundles_accounts_amount");
        el9.click();


        Thread.sleep(400);

        MobileElement el4 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfer_confirm_button"));
        el4.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/transfer_btn_confirm")));
        MobileElement el5 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfer_btn_confirm"));
        el5.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/ticket_title")));
        MobileElement el6 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/ticket_title"));
        Assert.assertTrue(el6.getText().equals("¡Transferencia exitosa!"));

        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }


    @Test
    public void validaCuentasDiferentes() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/account_btn_transfer")));
        MobileElement el2 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_btn_transfer"));
        el2.click();


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit")));
        MobileElement el3 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit"));
        el3.sendKeys("0.01");

        MobileElement el7 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_number_edit"));
        el7.sendKeys(ctaOrigen);
        Thread.sleep(300);
        MobileElement el4 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfer_confirm_button"));
        el4.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/transfer_btn_confirm")));
        MobileElement el5 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/transfer_btn_confirm"));
        el5.click();


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("android:id/message")));
        MobileElement el02 = (MobileElement) ad.findElement(By.id("android:id/message"));
        Assert.assertTrue(el02.getText().equals("La cuenta de origen debe ser diferente a la cuenta de destino"));

        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

}