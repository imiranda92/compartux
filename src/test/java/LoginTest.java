import io.appium.java_client.MobileElement;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;

public class LoginTest extends TestBase {
    public String path= "/Users/gesfor-g0088/Desktop/EvidenciasComparTfon/Login/";

    @BeforeClass
    public void setUP() throws IOException {
        File file = new File(path);
        FileUtils.deleteDirectory(file);
        java.io.File folder = new File(path);
        folder.mkdir();

    }
    @Test //Valodar Login de cliente
    public void validaLoginCliente () throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
        MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/password_edt");
        el2.clear();
        el2.sendKeys(password);
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_btn");
        el3.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/start_que_quieres_hacer")));
        Assert.assertEquals(ad.findElementById("com.fiinlab.compartfon:id/start_que_quieres_hacer").getText(),"¿Qué quieres hacer?");
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
        Thread.sleep(2000);
    }

    @Test
    public void validaContraseñaIncorrecta() throws InterruptedException, IOException {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
        MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/password_edt");
        el2.clear();
        el2.sendKeys(password+"not");
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_btn");
        el3.click();
        Thread.sleep(500);
        wait.until(ExpectedConditions.elementToBeClickable(el3));
        Assert.assertTrue(el3.isEnabled());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
        Thread.sleep(2000);
    }

    @Test
    public void validaCamposObligatorios () throws InterruptedException, IOException {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
        MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/password_edt");
        el2.clear();
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_btn");
        el3.click();
        Assert.assertFalse(el3.isEnabled());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
        Thread.sleep(2000);
    }





}