import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;


public class TestBase{

    AndroidDriver<MobileElement> ad;
    DesiredCapabilities dc;

    //Juan
    //public  String numCel="5514251366";
    //public  String numTDD="4572498120967811";
    //public  String password="RAICHU69a";

    //Itzel
    public  String numCel="5537478432";
    public  String numTDD="4572498019541743";
    public  String password="RAICHU69a";
        //Fecha nacimiento
    public String birthDay= "20";
    public String birthMonth="08";
    public String birthYear="1992";

    //Datos de usuario con más de una cuenta
    public String celUserCuentas="3322609009";
    public String pswUserCuentas="Tuzmu199";
    //Datos para transferencias
    public String ctaDestino="00141675319";
    public String ctaOrigen="00144942675";
    public String montoTransfer="1.10";


    //TimeOut
    public int timeOut=160;

    //-----Web driver
    WebDriver driver;
    //locators
    By btnMexico = By.xpath("//img[@src='flags/flag-mexico.png']");
    By numCelular = By.id("btn");
    By msgCodigo = By.xpath("//*[contains(text(), '<#> Tu codigo')]");
    public int tiempoEspera=40;


    @BeforeMethod
    @Parameters({"UDID","URL_","version","systemPort"})
    public void setUp(@Optional("emulator-5554") String UDID, @Optional("http://127.0.0.1:4723/wd/hub") String URL_ , @Optional("7.0")String version,@Optional("8200") String systemPort) throws Exception {
        dc = new DesiredCapabilities();
        dc.setCapability("udid", UDID);
        dc.setCapability("deviceName", UDID);
        dc.setCapability("platformName","android");
        dc.setCapability(AndroidMobileCapabilityType.SYSTEM_PORT, systemPort);
        dc.setCapability("version",version);
        dc.setCapability("app","/Users/gesfor-g0088/Downloads/compartfon-móvil_2.0.2.apk");
        dc.setCapability("appPackage", "com.fiinlab.compartfon");
        dc.setCapability("appActivity", "com.fiinlab.compartfon.splash.SplashScreenActivity");
        dc.setCapability("platformVersion",version);

        //Reset app
        dc.setCapability("noReset", "true");
        dc.setCapability("fullReset", "false");

        //Para solucionar error pop ups
        dc.setCapability("autoGrantPermissions", true);
        dc.setCapability("automationName","UiAutomator2");
        ad = new AndroidDriver<MobileElement>(new URL(URL_),dc );
    }

    @AfterMethod
    public void tearDown() throws Exception {
        if(ad.getCapabilities().getVersion().equals("7.0")  || ad.getCapabilities().getVersion().equals("8.0")) {
            ad.pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
            Thread.sleep(2000);
            WebDriverWait wait = new WebDriverWait(ad,20);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.android.systemui:id/button")));
            MobileElement el1 = (MobileElement) ad.findElementById("com.android.systemui:id/button");
            el1.click();
            Thread.sleep(2000);
            ad.pressKey(new KeyEvent(AndroidKey.APP_SWITCH.HOME));
            Thread.sleep(2000);
            ad.pressKey(new KeyEvent(AndroidKey.BACK));
            Thread.sleep(2000);
            ad.pressKey(new KeyEvent(AndroidKey.BACK));
            ad.quit();
        }
        else { ad.quit();}
    }

    ///////////////////////////////////////////////////////////////////

    //refresh app
    public void refresh() throws InterruptedException {
        Thread.sleep(2000);
        ad.pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
        Thread.sleep(2000);
        System.out.println();
        if (ad.getCapabilities().getVersion().equals("7.0") || ad.getCapabilities().getVersion().equals("8.0")){// Element el2 para emulador version 7.0
            MobileElement el2 = (MobileElement) ad.findElementById("com.android.systemui:id/task_view_thumbnail");
            el2.click();}
        else if (ad.getCapabilities().getVersion().equals("9.0")){// Element el2 para emulador version 7.0
            MobileElement el2 = (MobileElement) ad.findElementByXPath("//android.widget.FrameLayout[@content-desc=\"Compartfon\"]/android.view.View");
            el2.click();}
        Thread.sleep(1000);

    }
    //ScreenShots path y nombre de la captura  /Users/gesfor-g0088/Desktop/EvidenciasComparTfon/
    public void screenShot(String path,String nombreScrnShot) throws IOException {
        File scr=((TakesScreenshot)ad).getScreenshotAs(OutputType.FILE);
        File dest= new File(path + nombreScrnShot+"_"+timestamp()+".png");
        FileHandler.copy(scr,dest);
    }
    //ScreenShots sin path
    public void screenShot(String nombreScrnShot) throws IOException {
        File scr=((TakesScreenshot)ad).getScreenshotAs(OutputType.FILE);
        File dest= new File("C:/Users/GESFOR-667/Downloads/Compartfon/ScrShots/"+ nombreScrnShot+timestamp()+".png");
        FileHandler.copy(scr,dest);
    }
    //Obtener fecha (Time stamp)
    public String timestamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
    }
    //Login
    public void logIn() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
        MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/password_edt");
        el2.clear();
        el2.sendKeys(password);
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_btn");
        el3.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/start_que_quieres_hacer")));
        Thread.sleep(2000);
    }



    public void logIn(String psw) throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
        MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/password_edt");
        el2.clear();
        el2.sendKeys(psw);
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_btn");
        el3.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/start_que_quieres_hacer")));
        Thread.sleep(2000);
    }
    //Validar si esta loggeado
    public void checkIfLogged() throws Exception {
        if (existsElement("id","com.fiinlab.compartfon:id/txt_sms_informacion")== false) {
            if (existsElement("id","com.fiinlab.compartfon:id/login_usuario_edt")){
                logIn();
            }else {
                ad.launchApp();
                logIn();}
        }
    }
    public void checkIfLogged(String celTDD, String psw) throws Exception {
        if (existsElement("id","com.fiinlab.compartfon:id/txt_sms_informacion")== false) {
            if (existsElement("id","com.fiinlab.compartfon:id/login_usuario_edt")){
                logIn();
            }else {
                ad.launchApp();
                logIn(psw);}
        }
    }
    //Validar si un elemento existe
    public boolean existsElement(String type,String string ) {
        if (type == "id") {
            try {
                ad.findElement(By.id(string));
            } catch (NoSuchElementException e) {
                return false;
            }
        }

        if (type == "xpath") {
            try {
                ad.findElement(By.xpath(string));
            } catch (NoSuchElementException e) {
                return false;
            }
        }

        return true;
    }
    //Cerrar Tabs abiertas en Chrome
    public void cerrarTabsChrome() throws Exception {
        MobileElement el15 = (MobileElement) ad.findElementById("com.android.chrome:id/tab_switcher_button");
        int pestañasabiertas= Character.getNumericValue( el15.getAttribute("contentDescription").charAt(0));
        el15.click();
        PointOption p1 = new PointOption();
        PointOption p2 = new PointOption();
        p1.withCoordinates(259,420);
        p2.withCoordinates(813,431);
        Thread.sleep(1000);
        for (int i = 0; i <= pestañasabiertas+2 ; i++) {
            swipe(p1,p2);
            Thread.sleep(1500);
        }
    }

    /////////////////Obtener insumos del cliente///////////////////

    //Obtener el descuento asociado al cliente
    public int obtenerDescuento() throws Exception {
        checkIfLogged();
        MobileElement el1 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.ScrollView/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.RelativeLayout");
        el1.click();
        //refresh();
        WebDriverWait wait = new WebDriverWait(ad,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/telefonia_descuentos")));
        MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/telefonia_descuentos");
        return Integer.valueOf(el2.getText().substring(17,19));
    }
    //Obtener el saldo asociado de un cliente en una cuenta especifica
    public float consultaSaldo(String cuentaABuscar) throws Exception {
        String cuentaABuscarXpath= validaCuentaExiste(cuentaABuscar);
        MobileElement el2 = (MobileElement) ad.findElementByXPath(cuentaABuscarXpath);
        el2.click();
        //refresh();
        WebDriverWait wait = new WebDriverWait(ad,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/operaciones_banco_saldo_disponible_txt")));
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/operaciones_banco_saldo_disponible_txt");
        Float Saldo = Float.valueOf(el3.getText().substring(1));
        ad.pressKey(new KeyEvent(AndroidKey.BACK));
        Thread.sleep(1000);
        ad.pressKey(new KeyEvent(AndroidKey.BACK));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_sms_informacion")));
        //refreshApp();
        return Saldo;
    }
    //Consulta el saldo de la primera cuenra
    public float consultaSaldo( ) throws Exception {
        consultaPrimeraCuenta();
        WebDriverWait wait = new WebDriverWait(ad,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/operaciones_banco_saldo_disponible_txt")));
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/operaciones_banco_saldo_disponible_txt");
        Float Saldo = Float.valueOf(el3.getText().substring(1));
        ad.pressKey(new KeyEvent(AndroidKey.BACK));
        Thread.sleep(1000);
        //refreshApp();
        return Saldo;
    }
    //Valida si una cuenta especifica existe
    public String validaCuentaExiste(String cuentaABuscar) throws Exception {
        int i=1;
        String cuentaABuscarXpath ="";
        checkIfLogged();
        MobileElement el1 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/menu_img_shop");
        el1.click();
        //refreshApp();
        Thread.sleep(2000);

        String xpathCuentas= "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.TextView";
        if (existsElement("xpath",xpathCuentas)) {
            do{
                if(ad.findElementByXPath(xpathCuentas).getText().equals(cuentaABuscar)){
                    cuentaABuscarXpath=xpathCuentas;
                }
                i++;
                xpathCuentas = xpathCuentas.substring(0, 236) + i + xpathCuentas.substring(237);
            }while (existsElement("xpath",xpathCuentas));
        }
        return cuentaABuscarXpath;
    }

    //Obtener el primer movimiento mostrado, como parametro es la opción de la lista de valores, ejemplo 1=7 días, 2=14 días..
    public MobileElement consultaMovs(int opcion ) throws Exception {
        checkIfLogged();
        MobileElement el1 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/menu_img_shop");
        el1.click();
        WebDriverWait wait = new WebDriverWait(ad,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/operaciones_banco_saldo_disponible_txt")));

        MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/operaciones_banco_movimientos_btn");
        el2.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/movimientos_filtro_spinner")));
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/movimientos_filtro_spinner");
        el3.click();
        Thread.sleep(500);
        String xpathSpinner= "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[";
        //Select num días
        MobileElement el4 = (MobileElement) ad.findElementByXPath(xpathSpinner+opcion+"]");
        el4.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView[3]")));
        MobileElement el5 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView[3]");
        return  el5;
    }

    //Entrar a la primera cuenta del cliente (o única cuenta en caso de no tener más)
    public void consultaPrimeraCuenta() throws Exception {
        checkIfLogged();
        MobileElement el1 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/menu_img_shop");
        el1.click();
        Thread.sleep(5000);
        WebDriverWait wait = new WebDriverWait(ad,20);
        String xpathCuenta= "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.TextView";
        //Entra en if si el cliente tiene más de una cuenta, de lo contrario este elemento no existe ya que debe entrar en automático
        if (existsElement("xpath",xpathCuenta)) {
            MobileElement el2 = (MobileElement) ad.findElementByXPath(xpathCuenta);
            el2.click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/operaciones_banco_saldo_disponible_txt")));
        }else {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/operaciones_banco_saldo_disponible_txt")));
        }
    }

    //Relizar transferencia
    public void realizaTransferencia(String cta, String monto) throws Exception {
        consultaPrimeraCuenta();
        //Transfer
        MobileElement el1 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/operaciones_banco_transferencias_btn");
        el1.click();
        WebDriverWait wait = new WebDriverWait(ad,20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/ctaDestino")));
        MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/ctaDestino");
        el2.clear();
        el2.setValue(cta);
        Thread.sleep(500);
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/monto");
        el3.clear();
        el3.setValue(monto);
        Thread.sleep(300);
        MobileElement el4 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/btnTranserencia");
        el4.click();
        Thread.sleep(200);
    }


    /////////////////////////////////////////////////
    //Obtener nombre del mes actual
    public String monthName(Boolean ingles) {
        String [] monthName;
        if (ingles) {
            monthName = new String[]{"January", "February",
                    "March", "April", "May", "June", "July",
                    "August", "September", "October", "November",
                    "December"};
        }else {
            monthName = new String[]{"Enero", "Febrero",
                    "Marzo", "Abril", "Mayo", "Junio", "Julio",
                    "Agosto", "Septiembre", "Octubre", "Noviembre",
                    "Diciembre"};
        }
        Calendar cal = Calendar.getInstance();

        String month = monthName[cal.get(Calendar.MONTH)];
        return  month;
    }
    //Obtener número de mes
    public int monthNumber(String target, Boolean ingles) {
        String [] monthName;
        if (ingles) {
            monthName = new String[]{"January", "February",
                    "March", "April", "May", "June", "July",
                    "August", "September", "October", "November",
                    "December"};
        }
        else {
            monthName = new String[]{"Enero", "Febrero",
                    "Marzo", "Abril", "Mayo", "Junio", "Julio",
                    "Agosto", "Septiembre", "Octubre", "Noviembre",
                    "Diciembre"};
        }
        return ArrayUtils.indexOf(monthName, target);
    }
    //Swipe de punto1 a punto2
    public  void swipe(PointOption p1, PointOption p2){
        TouchAction ta = new TouchAction(ad);
        ta.press(p1).waitAction(WaitOptions.waitOptions(Duration.ofMillis(500))).moveTo(p2).release().perform();
    }
    //Metodos calendario para Samsung galaxy nota 9 (Androiod 9)
    public void swipeYear(Boolean añoSiguiente, int movimientos) throws InterruptedException {
        PointOption p1 = new PointOption();
        PointOption p2 = new PointOption();
        if (añoSiguiente){
            p1.withCoordinates(936,1410);
            p2.withCoordinates(1020,1260);
        }else {
            p1.withCoordinates(936,1260);
            p2.withCoordinates(1020,1410);
        }
        for (int i = 0; i < movimientos; i++) {
            swipe(p1,p2);
            Thread.sleep(1000);
        }
    }
    public void swipeMonth(Boolean mesSiguiente, int movimientos) throws InterruptedException {
        PointOption p1 = new PointOption();
        PointOption p2 = new PointOption();
        if (mesSiguiente){
            p1.withCoordinates(936-310,1360);
            p2.withCoordinates(1020-310,1314);
        }else {
            p1.withCoordinates(936-310,1314);
            p2.withCoordinates(1020-310,1360);
        }
        for (int i = 0; i < movimientos; i++) {
            swipe(p1,p2);
            Thread.sleep(1000);
        }
    }
    public void swipeDay(Boolean diaSiguiente, int movimientos) throws InterruptedException {
        PointOption p1 = new PointOption();
        PointOption p2 = new PointOption();
        if (diaSiguiente){
            p1.withCoordinates(368,1360);
            p2.withCoordinates(433,1314);
        }else {
            p1.withCoordinates(369,1314);
            p2.withCoordinates(433,1360);
        }
        for (int i = 0; i < movimientos; i++) {
            swipe(p1,p2);
            Thread.sleep(1000);
        }
    }
    public void setMonth(MobileElement element, String mes) throws InterruptedException {

        int valueMonth = monthNumber(element.getText(),false);
        int desiredMonth = monthNumber(mes,false);
        do{
            if (valueMonth > desiredMonth ) {
                swipeMonth(false, 1);
            }
            if (valueMonth < desiredMonth ) {
                swipeMonth(true, 1);
            }
            valueMonth = monthNumber(element.getText(),false);
        }while ( valueMonth != desiredMonth);
    }
    public void setYear(MobileElement element, int año) throws InterruptedException {
        int valueYear = Integer.valueOf(element.getText());
        do{
            if (valueYear > año ) {
                swipeYear(false, 1);
            }
            if (valueYear < año ) {
                swipeYear(true, 1);
            }
            valueYear = Integer.valueOf(element.getText());
        }while ( valueYear != año);
    }
    public void setDay(MobileElement element, int day) throws InterruptedException {
        int valueDay = Integer.valueOf(element.getText());
        do{
            if (valueDay > day ) {
                swipeDay(false, 1);
            }
            if (valueDay < day ) {
                swipeDay(true, 1);
            }
            valueDay = Integer.valueOf(element.getText());
        }while ( valueDay != day);
    }
    public Boolean checkDate(MobileElement elDay, MobileElement elMonth, MobileElement elYear, int day, String month, int year){
        int valueDay = Integer.valueOf(elDay.getText());
        int valueYear = Integer.valueOf(elYear.getText());
        if (valueDay == day && valueYear== year && elMonth.getText().equals(month)){
            return true;
        }else {
            return  false;
        }
    }
    public void setDate(MobileElement elDay, MobileElement elMonth, MobileElement elYear, int day, String month, int year) throws InterruptedException {
        do{
            setDay(elDay,day);
            setMonth(elMonth,month);
            setYear(elYear,year);
        }while ( checkDate(elDay,elMonth,elYear,day,month,year) == false);
    }
    //Para calendario emulador (Nexus 5 Android 7 API 24)
    public void pickDay(int day){
        // Year
        MobileElement btnAño = (MobileElement) ad.findElementById("android:id/date_picker_header_year");
        String year = btnAño.getText();
        //Month
        String month = monthName(true);
        //Select day
        MobileElement btnDia = (MobileElement) ad.findElementByAccessibilityId(day +" "+month+" "+year);
        btnDia.click();
        MobileElement el2 = (MobileElement) ad.findElementById("android:id/button1");
        el2.click();
    }
    public void changeYear(Boolean añoSiguiente,int movimientos) throws InterruptedException {
        MobileElement btnAño = (MobileElement) ad.findElementById("android:id/date_picker_header_year");
        btnAño.click();
        Thread.sleep(1000);
        swipeYear(añoSiguiente,movimientos);
        MobileElement el1 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.DatePicker/android.widget.LinearLayout/android.widget.ViewAnimator/android.widget.ListView/android.widget.TextView[3]");
        el1.click();
    }
    /////////////////////////////////////////////////////

    public void deleteFolder(File fileDel) {
        if(fileDel.isDirectory()){

            if(fileDel.list().length == 0)
                fileDel.delete();
            else{

                for (String temp : fileDel.list()) {
                    File fileDelete = new File(fileDel, temp);
                    //recursive delete
                    deleteFolder(fileDelete);
                }

                //check the directory again, if empty then delete it
                if(fileDel.list().length==0)
                    fileDel.delete();

            }

        }else{

            //if file, then delete it
            fileDel.delete();
        }
    }

}