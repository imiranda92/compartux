import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.List;

import static org.testng.Assert.*;

public class NuevaContraseñaTest extends TestBase{
    public String path= "/Users/gesfor-g0088/Desktop/EvidenciasComparTfon/Nueva Contraseña/";

    @BeforeClass
    public void setUP() throws IOException {
        File file = new File(path);
        FileUtils.deleteDirectory(file);
        java.io.File folder = new File(path);
        folder.mkdir();

    }
    @Test
    public void validaNumTdd() throws InterruptedException, IOException {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/login_forgot_txt")));
        MobileElement el1 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_forgot_txt");
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_card_1")));
        for (int i = 0; i < 4; i++) {
            String tdd=numTDD.substring(i*4,(i+1)*4);
            MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_"+(i+1));
            el2.clear();
            el2.sendKeys(tdd);
        }
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_card_next")));
        Assert.assertTrue(ad.findElementById("com.fiinlab.compartfon:id/reset_card_next").isEnabled());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void validaEdad () throws InterruptedException, IOException {
        String dia= "24";
        String mes="01";
        String año="2010";

        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/login_forgot_txt")));
        MobileElement el1 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_forgot_txt");
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_card_1")));
        for (int i = 0; i < 4; i++) {
            String tdd=numTDD.substring(i*4,(i+1)*4);
            MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_"+(i+1));
            el2.clear();
            el2.sendKeys(tdd);
        }
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_card_next")));
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_next");
        el3.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_birth_day")));
        MobileElement el4 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_day");
        MobileElement el5 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_month");
        MobileElement el6 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_year");

        el4.sendKeys(dia);
        el5.sendKeys(mes);
        el6.sendKeys(año);
        Thread.sleep(1000);
        Assert.assertFalse(ad.findElementById("com.fiinlab.compartfon:id/reset_birth_next").isEnabled());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void validaNumCel() throws InterruptedException, IOException {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/login_forgot_txt")));
        MobileElement el1 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_forgot_txt");
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_card_1")));
        for (int i = 0; i < 4; i++) {
            String tdd=numTDD.substring(i*4,(i+1)*4);
            MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_"+(i+1));
            el2.clear();
            el2.sendKeys(tdd);
        }
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_card_next")));
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_next");
        el3.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_birth_day")));
        MobileElement el4 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_day");
        MobileElement el5 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_month");
        MobileElement el6 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_year");

        el4.sendKeys(birthDay);
        el5.sendKeys(birthMonth);
        el6.sendKeys(birthYear);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_birth_next")));
        MobileElement el7 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_next");
        el7.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_phone_edit")));
        MobileElement el8 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_phone_edit");
        el8.sendKeys(numCel);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_phone_next")));
        Assert.assertTrue(ad.findElementById("com.fiinlab.compartfon:id/reset_phone_next").isEnabled());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void validaSolicitudCodigo() throws InterruptedException, IOException {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/login_forgot_txt")));
        MobileElement el1 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_forgot_txt");
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_card_1")));
        for (int i = 0; i < 4; i++) {
            String tdd=numTDD.substring(i*4,(i+1)*4);
            MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_"+(i+1));
            el2.clear();
            el2.sendKeys(tdd);
        }
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_card_next")));
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_next");
        el3.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_birth_day")));
        MobileElement el4 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_day");
        MobileElement el5 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_month");
        MobileElement el6 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_year");

        el4.sendKeys(birthDay);
        el5.sendKeys(birthMonth);
        el6.sendKeys(birthYear);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_birth_next")));
        MobileElement el7 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_next");
        el7.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_phone_edit")));
        MobileElement el8 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_phone_edit");
        el8.sendKeys(numCel);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_phone_next")));
        MobileElement el9 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_phone_next");
        el9.click();

        wait.until((ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_code_1"))));
        MobileElement el10 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_code_1");
        Assert.assertTrue(el10.isDisplayed());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void validaCodigoErroneo() throws InterruptedException, IOException {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/login_forgot_txt")));
        MobileElement el1 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_forgot_txt");
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_card_1")));
        for (int i = 0; i < 4; i++) {
            String tdd=numTDD.substring(i*4,(i+1)*4);
            MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_"+(i+1));
            el2.clear();
            el2.sendKeys(tdd);
        }
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_card_next")));
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_next");
        el3.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_birth_day")));
        MobileElement el4 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_day");
        MobileElement el5 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_month");
        MobileElement el6 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_year");

        el4.sendKeys(birthDay);
        el5.sendKeys(birthMonth);
        el6.sendKeys(birthYear);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_birth_next")));
        MobileElement el7 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_next");
        el7.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_phone_edit")));
        MobileElement el8 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_phone_edit");
        el8.sendKeys(numCel);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_phone_next")));
        MobileElement el9 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_phone_next");
        el9.click();


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_code_next")));
        for (int i = 0; i < 6; i++) {
            MobileElement el10 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_code_"+(i+1));
            el10.sendKeys("0");
        }

        wait.until((ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_code_next"))));
        MobileElement el11 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_code_next");
        el11.click();
        Thread.sleep(950);
        Assert.assertTrue(el11.isEnabled());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void validaCelNoRegistrado() throws InterruptedException, IOException {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/login_forgot_txt")));
        MobileElement el1 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_forgot_txt");
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_card_1")));
        for (int i = 0; i < 4; i++) {
            String tdd=numTDD.substring(i*4,(i+1)*4);
            MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_"+(i+1));
            el2.clear();
            el2.sendKeys(tdd);
        }
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_card_next")));
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_next");
        el3.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_birth_day")));
        MobileElement el4 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_day");
        MobileElement el5 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_month");
        MobileElement el6 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_year");

        el4.sendKeys(birthDay);
        el5.sendKeys(birthMonth);
        el6.sendKeys(birthYear);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_birth_next")));
        MobileElement el7 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_next");
        el7.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_phone_edit")));
        MobileElement el8 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_phone_edit");
        el8.sendKeys("0101230098");
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_phone_next")));
        MobileElement el9 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_phone_next");
        el9.click();
        Thread.sleep(1600);
        Assert.assertFalse(ad.findElementById("com.fiinlab.compartfon:id/reset_phone_next").isEnabled());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
    }

    @Test
    public void validaEnvioNuevoSMS() throws InterruptedException, IOException {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/login_forgot_txt")));
        MobileElement el1 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/login_forgot_txt");
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_card_1")));
        for (int i = 0; i < 4; i++) {
            String tdd=numTDD.substring(i*4,(i+1)*4);
            MobileElement el2 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_"+(i+1));
            el2.clear();
            el2.sendKeys(tdd);
        }
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_card_next")));
        MobileElement el3 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_card_next");
        el3.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_birth_day")));
        MobileElement el4 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_day");
        MobileElement el5 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_month");
        MobileElement el6 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_year");

        el4.sendKeys(birthDay);
        el5.sendKeys(birthMonth);
        el6.sendKeys(birthYear);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_birth_next")));
        MobileElement el7 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_birth_next");
        el7.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_phone_edit")));
        MobileElement el8 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_phone_edit");
        el8.sendKeys(numCel);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_phone_next")));
        MobileElement el9 = (MobileElement) ad.findElementById("com.fiinlab.compartfon:id/reset_phone_next");
        el9.click();

        wait.until((ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/reset_code_1"))));

        WebDriverWait waitSMS = new WebDriverWait(ad, 120);

        waitSMS.until((ExpectedConditions.elementToBeClickable(By.id("com.fiinlab.compartfon:id/reset_code_link"))) );

        Assert.assertTrue(ad.findElementById("com.fiinlab.compartfon:id/reset_code_link").isEnabled());
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);

    }

}