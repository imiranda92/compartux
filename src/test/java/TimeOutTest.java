import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import static io.appium.java_client.touch.offset.PointOption.point;

public class TimeOutTest extends TestBase{

    public String path= "/Users/gesfor-g0088/Desktop/EvidenciasComparTfon/TimeOut/";

    @BeforeClass
    public void setUP() throws IOException {
        File file = new File(path);
        FileUtils.deleteDirectory(file);
        java.io.File folder = new File(path);
        folder.mkdir();

    }

    @Test
    public void timeOutInicio() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));

        WebDriverWait waitTimeOut = new WebDriverWait(ad,timeOut);


        waitTimeOut.until(ExpectedConditions.visibilityOfElementLocated(By.id("android:id/message")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("android:id/message"));
        Assert.assertTrue(el1.getText().equals("La sesión ha expirado por inactividad. Vuelva a iniciar sesión."));
        MobileElement el2 = (MobileElement) ad.findElement(By.id("android:id/button1"));
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);

        el2.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
    }

    @Test
    public void timeOutPaquetes() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(new MobileBy.ByAccessibilityId("Open navigation drawer")));
        MobileElement el0 = (MobileElement) ad.findElementByAccessibilityId("Open navigation drawer");
        el0.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/package_menu_btn")));
        MobileElement el01 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/package_menu_btn"));
        Thread.sleep(200);
        el01.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.TextView")));

        WebDriverWait waitTimeOut = new WebDriverWait(ad,timeOut);
        waitTimeOut.until(ExpectedConditions.visibilityOfElementLocated(By.id("android:id/message")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("android:id/message"));
        Assert.assertTrue(el1.getText().equals("La sesión ha expirado por inactividad. Vuelva a iniciar sesión."));
        MobileElement el2 = (MobileElement) ad.findElement(By.id("android:id/button1"));
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);

        el2.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
    }

    @Test
    public void timeOutDescuento() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(new MobileBy.ByAccessibilityId("Open navigation drawer")));
        MobileElement el1 = (MobileElement) ad.findElementByAccessibilityId("Open navigation drawer");
        el1.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/benefits_menu_btn")));
        MobileElement el01 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/benefits_menu_btn"));
        Thread.sleep(200);
        el01.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget." +
                "LinearLayout/android.widget.TextView[1]")));

        WebDriverWait waitTimeOut = new WebDriverWait(ad,timeOut);
        waitTimeOut.until(ExpectedConditions.visibilityOfElementLocated(By.id("android:id/message")));
        MobileElement el02 = (MobileElement) ad.findElement(By.id("android:id/message"));
        Assert.assertTrue(el02.getText().equals("La sesión ha expirado por inactividad. Vuelva a iniciar sesión."));
        MobileElement el2 = (MobileElement) ad.findElement(By.id("android:id/button1"));
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);

        el2.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
    }

    @Test
    public void timeOutTransferencia() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/account_btn_transfer")));
        MobileElement el2 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/account_btn_transfer"));
        el2.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/beneficiary_number_edit")));

        WebDriverWait waitTimeOut = new WebDriverWait(ad,timeOut);
        waitTimeOut.until(ExpectedConditions.visibilityOfElementLocated(By.id("android:id/message")));
        MobileElement el02 = (MobileElement) ad.findElement(By.id("android:id/message"));
        Assert.assertTrue(el02.getText().equals("La sesión ha expirado por inactividad. Vuelva a iniciar sesión."));
        MobileElement el03 = (MobileElement) ad.findElement(By.id("android:id/button1"));
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
        el03.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
    }

    @Test
    public void timeOutAhorros() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(new MobileBy.ByAccessibilityId("Ahorros")));
        MobileElement el2 = (MobileElement) ad.findElementByAccessibilityId("Ahorros");
        el2.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/linearLayout2")));
        ///
        WebDriverWait waitTimeOut = new WebDriverWait(ad,timeOut);
        waitTimeOut.until(ExpectedConditions.visibilityOfElementLocated(By.id("android:id/message")));
        MobileElement el02 = (MobileElement) ad.findElement(By.id("android:id/message"));
        Assert.assertTrue(el02.getText().equals("La sesión ha expirado por inactividad. Vuelva a iniciar sesión."));
        MobileElement el03 = (MobileElement) ad.findElement(By.id("android:id/button1"));
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
        el03.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
    }

    @Test
    public void timeOutMovimientos() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));

        ///
        WebDriverWait waitTimeOut = new WebDriverWait(ad,timeOut);
        waitTimeOut.until(ExpectedConditions.visibilityOfElementLocated(By.id("android:id/message")));
        MobileElement el02 = (MobileElement) ad.findElement(By.id("android:id/message"));
        Assert.assertTrue(el02.getText().equals("La sesión ha expirado por inactividad. Vuelva a iniciar sesión."));
        MobileElement el03 = (MobileElement) ad.findElement(By.id("android:id/button1"));
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
        el03.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
    }

    @Test
    public void timeOutDetelleCuenta() throws Exception {
        WebDriverWait wait = new WebDriverWait(ad,tiempoEspera);
        logIn();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/txt_account_name")));
        MobileElement el1 = (MobileElement) ad.findElement(By.id("com.fiinlab.compartfon:id/txt_account_name"));
        Thread.sleep(300);
        el1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.LinearLayout[2]")));


        wait.until(ExpectedConditions.visibilityOfElementLocated(new MobileBy.ByAccessibilityId("Información")));
        MobileElement el2 = (MobileElement) ad.findElementByAccessibilityId("Información");
        el2.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/accounts_info_txt_number")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/accounts_info_clabe")));

        ///
        WebDriverWait waitTimeOut = new WebDriverWait(ad,timeOut);
        waitTimeOut.until(ExpectedConditions.visibilityOfElementLocated(By.id("android:id/message")));
        MobileElement el02 = (MobileElement) ad.findElement(By.id("android:id/message"));
        Assert.assertTrue(el02.getText().equals("La sesión ha expirado por inactividad. Vuelva a iniciar sesión."));
        MobileElement el03 = (MobileElement) ad.findElement(By.id("android:id/button1"));
        String name = new Object(){}.getClass().getEnclosingMethod().getName();
        screenShot(path,name);
        el03.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.fiinlab.compartfon:id/password_edt")));
    }



}