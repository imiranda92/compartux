import static org.testng.Assert.*;

import java.awt.Color;
import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringTokenizer;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import org.testng.annotations.*;

import org.apache.commons.io.output.ChunkedWriter;

import com.itextpdf.text.pdf.PdfWriter;

public class ReportesPDF extends TestBase{

    public String nombreProyecto="ComparTfon UX/UI";
    public String pathFolder="/Users/gesfor-g0088/Desktop/EvidenciasComparTfon/";

    private static final Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 26, Font.BOLDITALIC);
    private static final Font paragraphFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL);
    private static final Font categoryFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static final Font subcategoryFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static final Font blueFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
    private static final Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);



    public  String encontrarCaso(String testCase) throws java.io.IOException
    {
        File file = new File("/Users/gesfor-g0088/Desktop/comparTfonUX/src/main/resources/Casos");

        FileReader fileR = null;
        BufferedReader file2 = null;
        String nombreCaso="";
        String descripcionCaso="";
        String resultEsperado="";
        String resultado="";

        try {
            fileR = new FileReader(file);
            file2 = new BufferedReader(fileR);


        } catch (FileNotFoundException e) {
            System.out.println("No se encontro el archivo "+file.getName());
        }

        try {
            String line="";
            while((line=file2.readLine())!=null)
            {
                StringTokenizer st = new StringTokenizer (line,"|");
                nombreCaso = st.nextToken();
                descripcionCaso = st.nextToken();
                resultEsperado=st.nextToken();

                if (nombreCaso.equals(testCase)){
                    resultado= ("Descripción del caso: "+descripcionCaso+"\nResultado esperado: "+resultEsperado+"\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(resultado);
        return resultado;
    }

    public static PdfPCell createImageCell(String path) throws DocumentException, IOException {
        Image img = Image.getInstance(path);
        img.setBorder(Rectangle.BOX);
        img.setBorderColor(BaseColor.WHITE);
        img.setBorderWidth(10);
        PdfPCell cell = new PdfPCell(img, true);
        //cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    public static PdfPCell createTextCell(String text) throws DocumentException, IOException {
        PdfPCell cell = new PdfPCell();
        Paragraph p = new Paragraph(text,paragraphFont);
        p.setAlignment(Element.ALIGN_LEFT);
        cell.addElement(p);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    @Test
    public void crearPDF() throws DocumentException, MalformedURLException, IOException {
        //Agregar dependencia de itext en pomp de maven
        //Crear un documento
        Document documento = new Document();
        Paragraph p = new Paragraph("\n");


        //Crear la instancia usando file output stream
        String nombreDocumento = "ReportePruebas"+timestamp()+".pdf";
        PdfWriter writer = PdfWriter.getInstance(documento,new FileOutputStream(nombreDocumento));
       // PdfWriter.getInstance(documento, new FileOutputStream(nombreDocumento));
        writer.setStrictImageSequence(true);


        //Abrir el documento
        documento.open();

        //Escribir datos
        Chunk titulo = new Chunk ("Reporte de pruebas "+nombreProyecto,FontFactory.getFont(FontFactory.HELVETICA_BOLD, 20, Font.NORMAL, new BaseColor(128,0, 128)));
        documento.add(titulo);

        //Folder

        File folder = new File(pathFolder);
        File[] listOfFiles = folder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return !name.equals(".DS_Store");
            }
        });


        for (File file : listOfFiles) {
            if (!file.isFile()) {
                System.out.println(file.getName());
                newLine2(documento,5);
                newLine(p,5);
                Chunk folderName = new Chunk ("Módulo:"+file.getName(),FontFactory.getFont(FontFactory.HELVETICA_BOLD, 16, Font.NORMAL, new BaseColor(128,128, 128)));
                documento.add(p);
                documento.add(folderName);
                documento.add(p);


                File folderInt = new File(pathFolder+file.getName());
                File[] listOfFilesInt = folderInt.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return !name.equals(".DS_Store");
                    }
                });

                //Dentro de sub folder
                for (File file1 : listOfFilesInt){
                    if (file1.isFile()){
                        System.out.println(file1.getName());

                        Path rutaImagen = Paths.get(pathFolder+file.getName()+"/"+file1.getName());
                        Image img= Image.getInstance(rutaImagen.toAbsolutePath().toString());
                        img.scaleAbsolute(200,310);
                        img.setBorder(Rectangle.BOX);
                        img.setBorderColor(BaseColor.RED);
                        img.setBorderWidth(4);


                        Paragraph nombreImagen = new Paragraph ("Nombre imagen: "+file1.getName());
                        newLine2(documento,3);
                        StringTokenizer st = new StringTokenizer (file1.getName(),"_");
                        String caseName = st.nextToken();

                        PdfPTable table = new PdfPTable(2);
                        table.setWidthPercentage(100);
                        table.setWidths(new int[]{1, 2});

                        table.addCell(createImageCell(pathFolder+file.getName()+"/"+file1.getName()));
                        table.addCell(createTextCell("Nombre imagen: " +file1.getName()+ "\n"+encontrarCaso(caseName)+"\n"));
                        documento.add(table);

                    }
                }

            }
        }


        documento.close();

    }


    private static void saltoLinea( Document documento,int saltos) throws DocumentException {
        Paragraph empty = new Paragraph();

        for (int i = 1; i <saltos; i++) {
            empty.add(new Paragraph(""));

        }

        documento.add(empty);
    }


    private static void newLine(Paragraph parrafo, int saltos) {

        for (int i = 1; i <saltos; i++) {
            parrafo.add(new Paragraph(""));

        }
    }

    private static void newLine2(Document documento, int saltos) throws DocumentException {
        Paragraph empty = new Paragraph();
        newLine(empty,saltos);
        documento.add(empty);

    }




}
