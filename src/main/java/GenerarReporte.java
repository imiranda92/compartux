import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;


public class GenerarReporte {

    public int tiempoMax=20;
    public String correo="tecGuru02@yopmaill.com";
    public static Document documento = new Document();
    public String nombreDocumento = "ReportePruebas.pdf";




    public static void main(String[] args) throws Exception {
        String nombreDocumento = "ReportePruebas.pdf";
        PdfWriter.getInstance(documento, new FileOutputStream(nombreDocumento));
        documento.open();
        //Escribir Título
        Chunk titulo = new Chunk ("Reporte de pruebas Oracle Account",FontFactory.getFont(FontFactory.HELVETICA_BOLD, 20, Font.NORMAL, new BaseColor(128,0, 128)));
        documento.add(titulo);



        documento.close();
    }


    public void agregarEvidencia(String name) throws DocumentException, MalformedURLException, IOException {
        newLine2(documento, 2);
        Paragraph parrafo = new Paragraph (name,FontFactory.getFont(FontFactory.HELVETICA, 20, Font.ITALIC, new BaseColor(128,128, 128)));
        documento.add(parrafo);
        newLine2(documento, 2);
        //Insertar imagen
        Path rutaImagen = Paths.get("C:\\Users\\TecGuru\\Desktop\\ScreenShots\\"+name+".jpg");
        Image img= Image.getInstance(rutaImagen.toAbsolutePath().toString());

        img.scaleAbsolute(200,200);
        documento.add(img);
    }



//    public void capturaPantalla(String name) throws IOException {
        //abrir la pantalla
        //driver.get("http://www.google.com");
        //Variable con nombre de la pantalla
        //String name = new Object(){}.getClass().getEnclosingMethod().getName();
        //String inicioPantalla="Inicio.png";
        //Directorio
        //String directorio = "C:\\Users\\TecGuru\\Desktop\\ScreenShots\\";
        //Tomar captura
        //El casting es adaptar un objeto a otro sin perder la esencia del primero
        //File archivo= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        //FileUtils.copyFile(archivo, new File(directorio+name+".jpg"));
//    }


    public void crearPDF() throws DocumentException, MalformedURLException, IOException {
        //Agregar dependencia de itext en pomp de maven
        //Crear un documento
        Document documento = new Document();

        //Crear la instancia usando file output stream
        String nombreDocumento = "miprimerPDF.pdf";
        PdfWriter.getInstance(documento, new FileOutputStream(nombreDocumento));

        //Abrir el documento
        documento.open();
        //Escribir datos
        Chunk titulo = new Chunk ("Este es mi título",FontFactory.getFont(FontFactory.HELVETICA_BOLD, 20, Font.NORMAL, new BaseColor(128,0, 128)));
        documento.add(titulo);
        newLine2(documento, 20);
        //Insertar imagen

        Path rutaImagen = Paths.get("C:\\Users\\TecGuru\\Desktop\\ferret.jpg");
        Image img= Image.getInstance(rutaImagen.toAbsolutePath().toString());

        img.scaleAbsolute(200,200);
        img.setBorder(Rectangle.BOX);
        img.setBorderColor(BaseColor.RED);
        img.setBorderWidth(4);
        documento.add(img);
        newLine2(documento,18);
        //Escribir parrafos

        Paragraph parrafo = new Paragraph ("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent elit urna, dapibus quis neque sit amet, fringilla faucibus odio. Proin odio nisi, lacinia.",FontFactory.getFont(FontFactory.HELVETICA, 20, Font.ITALIC, new BaseColor(128,128, 128)));
        documento.add(parrafo);
        //Entre cada elemento(imagen título, parrafos,etc... debemos poner salto de linea

        //Cerrar el documento
        documento.close();

    }


    private static void saltoLinea( Document documento,int saltos) throws DocumentException {
        Paragraph empty = new Paragraph();

        for (int i = 1; i <saltos; i++) {
            empty.add(new Paragraph(""));

        }

        documento.add(empty);
    }


    private static void newLine(Paragraph parrafo, int saltos) {

        for (int i = 1; i <saltos; i++) {
            parrafo.add(new Paragraph(""));

        }
    }

    private static void newLine2(Document documento, int saltos) throws DocumentException {
        Paragraph empty = new Paragraph();
        newLine(empty,saltos);
        documento.add(empty);

    }
}
